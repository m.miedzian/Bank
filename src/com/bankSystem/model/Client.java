package com.bankSystem.model;

import java.io.Serializable;

public class Client implements Serializable {

    private String 	name;
    private String surname;
    private int id;
    private String pesel;
    private String address;
    private Account accountObject;



    public Client(String name, String surname, String pesel, String address,  int lastID) {

        this.name=name;
        this.surname=surname;
        this.pesel=pesel;
        this.address=address;
        this.id= lastID + 1;
        this.accountObject = new Account(this);
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getID() {
        return this.id;
    }

    public Account getAccount() {
        return this.accountObject;
    }

    public String getPesel() {
        return this.pesel;
    }

    public String getAddress() {
        return this.address;
    }
}

