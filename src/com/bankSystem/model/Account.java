package com.bankSystem.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bankSystem.data.DataBaseInFile;
import com.bankSystem.logic.SearchEngine;

public class Account implements Serializable {
	
	private static boolean accountSynchronized=false;
    private int accountBalance;
    private int accountNumber;
    private String accountHistory;

    public Account(Client object) {
        this.accountBalance = 0;
        this.accountNumber = object.getID() + 100;
        this.accountHistory = "Utworzenie konta\n";
    }

    public void paymentOnAccount (int money) {

                accountSynchronized = true;
                this.accountBalance = this.accountBalance + money;

                this.addToHistory("Wpłata środków na konto: +", money);
                accountSynchronized = false;


    }
    public boolean paymentFromAccount (int money) {
        if(this.accountBalance < money) {
            return false;
        }
        else {
        	accountSynchronized = true; 
            this.accountBalance = this.accountBalance - money;
            this.addToHistory("Wypłata środków z konta: -",money);
            accountSynchronized = false;
            return true;

        }

    }

    public boolean transfer (DataBaseInFile currentBase, int accountNumber, int money) {
        if(this.accountBalance < money) {
            return false;
        }
        else {
        	accountSynchronized = true;
            this.accountBalance = this.accountBalance - money;
            System.out.println(this.getAccountNumber());

            currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().accountBalance =
                    currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().accountBalance + money;

            System.out.println(currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().getAccountNumber());
            this.addToHistory("Przelew środków z konta: -",money);
            currentBase.getClientByID(SearchEngine.searchIdByAccountNumber(currentBase, accountNumber)).getAccount().addToHistory("Przelew środków na konto: +",money);
            accountSynchronized = false;
            return true;
        }

    }

    public void addToHistory (String history, int money) {
    	accountSynchronized = true;
        StringBuilder sb = new StringBuilder();
        sb = new StringBuilder();
        sb.append(money);
        String strInt = sb.toString();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));

        this.accountHistory+="\n" + "---" + "\n" + history + strInt + "zł - " + dateFormat.format(date);

        accountSynchronized = false;
    }

    public int getAccountNumber () {
        return this.accountNumber;
    }

    public int getAccountBalance () {
        return this.accountBalance;
    }

    public String getAccountHistory () {
        return this.accountHistory;
    }
    public static boolean getAccountSynchronizedStatus() {
    	return accountSynchronized;
    }
}