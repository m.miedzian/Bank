package com.bankSystem.data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import com.bankSystem.model.Client;

public class DataBaseInFile {

    private List<Client> clientsArray;
    private Integer lastID;


    public DataBaseInFile() {
        clientsArray = new ArrayList<>();
        lastID = 0;

    }

    public synchronized void addClient(String name, String surname, String pesel, String address) {
        clientsArray.add(new Client(name, surname, pesel, address,  this.lastID));
        lastID++;

    }

    public synchronized void removeClientByID(int ID) {

        clientsArray.remove(getClientByID(ID));
    }

    public synchronized void saveDataBase(String nameOfFile) throws IOException {

        ObjectOutputStream ob = null;

        try {
            ob=new ObjectOutputStream(new FileOutputStream("DataBase/" + nameOfFile));

            ob.writeObject(this.lastID);

            for(int i = 0; i < clientsArray.size(); i++) {
                ob.writeObject(clientsArray.get(i));
            }

            ob.flush();
        }
        finally {
            if(ob != null)
                ob.close();
        }


    }

    public void loadDataBase(String nameOfFile) throws IOException,ClassNotFoundException {

        ObjectInputStream ob2 = null;

        clientsArray.clear();

        try{
            ob2 = new ObjectInputStream(new FileInputStream("DataBase/" + nameOfFile));

            this.lastID = (Integer)ob2.readObject();

            while(true){

                clientsArray.add((Client)ob2.readObject());

            }
        }
        catch (EOFException ex) {

        }
        catch (FileNotFoundException e) {
        	this.saveDataBase(nameOfFile);
        
        }
        finally{

            if(ob2 != null)
                ob2.close();
        }

    }

    public int getNumberOfClients() {

        return this.clientsArray.size();
    }

    public Client getClient(int index) {

        return this.clientsArray.get(index);
    }

    public Client getClientByID(int ID) {

        for(int i=0; i<this.clientsArray.size();i++) {

            if(this.clientsArray.get(i).getID() == ID ) {
                return this.clientsArray.get(i);
            }
        }
        return null;
    }

    public int getLastID() {

        return this.lastID;
    }

}








