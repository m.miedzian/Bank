package com.bankSystem.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.bankSystem.model.Account;

public class ThreadForBackup implements Runnable {

    Thread t;
    DataBaseInFile base;
    Boolean closeThread=false;
    int sleepTime;
    
    public ThreadForBackup(DataBaseInFile currentBase, int time) {
        t = new Thread(this, "Backup");
        t.setPriority(5);
        t.start();
        base=currentBase;
        sleepTime=time*1000;

    }

    public void run() {
        try {

            while(!closeThread && !Account.getAccountSynchronizedStatus()){
                System.out.println("Zapis-------------------------------");
                
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
                Date date = new Date();
                
                base.saveDataBase("Backup/DataBase_Backup_" + dateFormat.format(date) );
                Thread.sleep(sleepTime);
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopThread() {
    	
    	closeThread=true;
    }
    
    
}