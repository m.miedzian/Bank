package com.bankSystem.ui;

import javafx.fxml.FXMLLoader;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

import java.io.IOException;

public class ConfirmBox {

    //Create variable
    boolean answer;

    public boolean display(String title, String message) throws IOException {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setResizable(false);

        Parent root = FXMLLoader.load(getClass().getResource("confirm.fxml"));

        window.setScene(new Scene(root));

        window.showAndWait();

        //Make sure to return answer
        return answer;
    }

}