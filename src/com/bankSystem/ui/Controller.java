package com.bankSystem.ui;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import com.bankSystem.data.DataBaseInFile;
import com.bankSystem.logic.SearchEngine;
import com.bankSystem.main.Main;
import com.bankSystem.ui.Controller;

public class Controller implements Initializable {


    @FXML TextField login;
    @FXML PasswordField haslo;
    @FXML Label info;
    @FXML Label idPole;
    @FXML Label error;
    @FXML Label saldo;
    @FXML Label info2;
    @FXML Label id2;
    @FXML TextField numer_konta;
    @FXML TextField kwota;
    @FXML Label pytanie;
    @FXML Label przelewInfo;
    @FXML Label info_dane;
    @FXML Label id_dane;
    @FXML TextField imie;
    @FXML TextField nazwisko;
    @FXML TextField id_klienta;
    @FXML TextField pesel;
    @FXML TextField numer_konta_dane;
    @FXML TextField adres;
    @FXML Label info_historia;
    @FXML Label id_historia;
    @FXML TextArea historia;
    @FXML Label error2;
    @FXML PasswordField haslo2;
    @FXML ListView lista;
    @FXML CheckBox check_imie;
    @FXML CheckBox check_nazwisko;
    @FXML CheckBox check_id;
    @FXML CheckBox check_pesel;
    @FXML CheckBox check_numer;
    @FXML CheckBox check_adres;
    @FXML TableView<ClientsTable> tabela;
    @FXML TableColumn kol_imie;
    @FXML TableColumn kol_nazwisko;
    @FXML TableColumn kol_id;
    @FXML TableColumn kol_nr;
    @FXML TableColumn kol_pesel;
    @FXML TableColumn kol_adres;
    @FXML TextField pole_imie;
    @FXML TextField pole_nazwisko;
    @FXML TextField pole_pesel;
    @FXML TextField pole_adres;
    @FXML Label nowy_imie;
    @FXML Label nowy_nazwisko;
    @FXML Label nowy_pesel;
    @FXML Label nowy_adres;
    @FXML Label nowy_id;
    @FXML Label nowy_nr;
    @FXML GridPane nowy_grid;
    @FXML Rectangle nowy_kwadrat;
    @FXML Label nowy_label;
    @FXML Label label_pesel;
    @FXML Label label_imie;
    @FXML Label label_nazwisko;
    @FXML Label label_id;
    @FXML Label label_nr;
    @FXML Label label_adres;
    @FXML TextField search;
    @FXML ChoiceBox wyszukiwanie_opcje;
    @FXML TextField nazwa_pliku;
    @FXML CheckBox data_pliku;
    @FXML TextField wplata_nr;
    @FXML TextField wplata_kwota;
    @FXML TextField przelew_z;
    @FXML TextField przelew_na;
    @FXML TextField przelew_kwota;


    static boolean answer=false;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        System.out.println("Init");
    }


    public void logInAsClient() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/ui.scenes/scene_login_client.fxml"));
        Scene scena2 = new Scene(root);
        Main.window.setScene(scena2);
    }

    public void logInAsEmployeer() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/ui.scenes/scene_login_employeer.fxml"));
        Scene scena2 = new Scene(root);
        Main.window.setScene(scena2);
    }

    public void displayById(DataBaseInFile Base, int ID) {

        info.setText(Base.getClientByID(ID).getName() + " " + Base.getClientByID(ID).getSurname());

        StringBuilder sb = new StringBuilder();
        sb.append(ID);
        String strInt = sb.toString();
        idPole.setText(strInt);
    }

    public void displayBalance(DataBaseInFile Base, int ID) {

        StringBuilder sb = new StringBuilder();
        sb.append(Base.getClientByID(ID).getAccount().getAccountBalance());
        String strInt = sb.toString();
        saldo.setText(strInt + " zł");
    }

    public void loginButton() throws IOException {

        try {

            Main.ID = Integer.parseInt(login.getText());
            String hasloWczytane = haslo.getText();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_client_side.fxml"));
            Parent parent = loader.load();
            Controller controller = (Controller) loader.getController();
            controller.displayById(Main.Base, Main.ID);
            controller.displayBalance(Main.Base, Main.ID);

            if (hasloWczytane.compareTo(Main.Base.getClientByID(Main.ID).getPesel()) == 0 || hasloWczytane.compareTo("admin") == 0) {

                Main.window.setScene(new Scene(parent));
            }

            else {

                throw new Exception();
            }
        }

        catch(Exception e) {

            login.setStyle("-fx-border-color: red");
            haslo.setStyle("-fx-border-color: red");
            login.clear();
            haslo.clear();
            error.setText("Niepoprawny login lub hasło!");
        }

    }

    public void loginButton2() throws IOException {

        try {

            String hasloWczytane = haslo2.getText();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side.fxml"));
            Parent parent = loader.load();

            if (hasloWczytane.compareTo("admin") == 0) {

                Main.window.setScene(new Scene(parent));
            }

            else {

                throw new Exception();
            }
        }

        catch(Exception e) {

            haslo2.setStyle("-fx-border-color: red");
            haslo2.clear();
            error2.setText("Niepoprawne hasło!");
        }

    }

    public void logout() throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("/ui.scenes/scene_index.fxml"));
        Main.window.setScene(new Scene(root));
        Main.ID=0;
    }

    public void windowTransfer() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_client_side_transfer.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.info2.setText(info.getText());
        controller.id2.setText(idPole.getText());
        Main.window.setScene(new Scene(parent));
    }

    public void doTransfer() throws IOException {
        try {

            int numer = Integer.parseInt(numer_konta.getText());
            int pieniadze = Integer.parseInt(kwota.getText());
            display("Potwierdzenie przelewu", "Czy chcesz dokonać przelewu?");

            if (answer) {

                if(Main.Base.getClientByID(Main.ID).getAccount().transfer(Main.Base, numer, pieniadze)) {
                    przelewInfo.setText("Przelew wykonany");
                    przelewInfo.setTextFill(Color.GREEN);
                    numer_konta.clear();
                    kwota.clear();
                    answer = false;
                }
                else {
                    przelewInfo.setText("Zbyt mało środków na koncie");
                    przelewInfo.setTextFill(Color.RED);
                    numer_konta.clear();
                    kwota.clear();
                    answer = false;
                }
            }

            else {

                throw new Exception();
            }
        }

        catch(Exception e) {

            przelewInfo.setText("Przelew nieudany!");
            przelewInfo.setTextFill(Color.RED);
            numer_konta.clear();
            kwota.clear();
        }
    }

    public void back() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_client_side.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.displayById(Main.Base, Main.ID);
        controller.displayBalance(Main.Base, Main.ID);
        Main.window.setScene(new Scene(parent));
    }


    public void searching() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_searching.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.tabela.setPlaceholder(new Label("Brak osób do wyświetlenia"));
        controller.wyszukiwanie_opcje.setItems(FXCollections.observableArrayList ("Imię", "Nazwisko", "Pesel", "Adres", "Numer konta", "Numer ID"));
        controller.wyszukiwanie_opcje.getSelectionModel().selectFirst();
        Main.window.setScene(new Scene(parent));
    }

    public void list() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_list.fxml"));

        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();
        controller.display2();

        Main.window.setScene(new Scene(parent));
    }

    public void copy() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_copy.fxml"));
        Parent parent = loader.load();

        Main.window.setScene(new Scene(parent));
    }

    public void makeCopy() throws IOException {

        String nazwa = "DataBase/Backup/" + nazwa_pliku.getText();

        if (data_pliku.isSelected()) {

            DateFormat dateFormat = new SimpleDateFormat("_yyyy-MM-dd_HH:mm:ss");
            Date date = new Date();

            nazwa+=dateFormat.format(date);
        }

        Main.Base.saveDataBase(nazwa);
        nazwa_pliku.clear();
        data_pliku.setSelected(false);
    }

    public void listDeleting() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_list_delete.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();
        controller.display2();

        Main.window.setScene(new Scene(parent));
    }

    public void back2() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side.fxml"));
        Parent parent = loader.load();
        Main.window.setScene(new Scene(parent));
    }

    public void addingNewClient() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_add.fxml"));
        Parent parent = loader.load();
        Main.window.setScene(new Scene(parent));
    }

    public void addNewClient() throws IOException {

        Main.Base.addClient(pole_imie.getText(), pole_nazwisko.getText(), pole_pesel.getText(), pole_adres.getText());
        pole_imie.clear();
        pole_nazwisko.clear();
        pole_pesel.clear();
        pole_adres.clear();

        nowy_grid.setVisible(true);
        nowy_label.setVisible(true);
        nowy_kwadrat.setVisible(true);
        label_imie.setVisible(true);
        label_nazwisko.setVisible(true);
        label_pesel.setVisible(true);
        label_id.setVisible(true);
        label_nr.setVisible(true);
        label_adres.setVisible(true);

        nowy_imie.setText(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getName());
        nowy_nazwisko.setText(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getSurname());
        nowy_pesel.setText(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getPesel());
        nowy_adres.setText(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getAddress());

        StringBuilder sb = new StringBuilder();
        sb.append(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getID());
        String strInt = sb.toString();
        nowy_id.setText(strInt);

        sb = new StringBuilder();
        sb.append(Main.Base.getClient(Main.Base.getNumberOfClients()-1).getAccount().getAccountNumber());
        strInt = sb.toString();
        nowy_nr.setText(strInt);
    }


    public void listOfCopies() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_list_OfCopies.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();
        controller.copiesDisplay();

        Main.window.setScene(new Scene(parent));
    }

    public void windowPaymentOn() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_paymentOn.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();

        Main.window.setScene(new Scene(parent));
    }

    public void paymentOn() throws IOException {

        Main.Base.getClientByID(SearchEngine.searchIdByAccountNumber(Main.Base, Integer.parseInt(wplata_nr.getText()))).getAccount().paymentOnAccount(Integer.parseInt(wplata_kwota.getText()));
        wplata_kwota.clear();
        wplata_nr.clear();
    }

    public void windowPaymentFrom() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_paymentFrom.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();

        Main.window.setScene(new Scene(parent));
    }

    public void paymentFrom() throws IOException {

        Main.Base.getClientByID(SearchEngine.searchIdByAccountNumber(Main.Base, Integer.parseInt(wplata_nr.getText()))).getAccount().paymentFromAccount(Integer.parseInt(wplata_kwota.getText()));
        wplata_kwota.clear();
        wplata_nr.clear();
    }

    public void windowEmployeerTransfer() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_employeer_side_transfer.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller)loader.getController();

        Main.window.setScene(new Scene(parent));
    }

    public void transfer() throws IOException {

        Main.Base.getClientByID(SearchEngine.searchIdByAccountNumber(Main.Base, Integer.parseInt(przelew_z.getText()))).getAccount().transfer(Main.Base, Integer.parseInt(przelew_na.getText()), Integer.parseInt(przelew_kwota.getText()));
        przelew_kwota.clear();
        przelew_z.clear();
        przelew_na.clear();
    }

    public void removeClient() throws IOException {

        System.out.println(tabela.getSelectionModel().getSelectedIndices());

        ClientsTable client = tabela.getSelectionModel().getSelectedItem();
        System.out.println(client.getId());

        display("Usunięcie klienta z bazy", "Czy na pewno chcesz usunąć klienta:\n" + client.getName() + " " + client.getSurname() + "\n z bazy na stałe?");

        if (answer) {

            Main.Base.removeClientByID(client.getId());
            display2();
            }
    }

    public void display2() {

        ObservableList<ClientsTable> items2 = FXCollections.observableArrayList ();

        for(int i=0;i<Main.Base.getNumberOfClients();i++) {

            items2.add(new ClientsTable(Main.Base.getClient(i)));
        }

        kol_imie.setVisible(false);
        kol_nazwisko.setVisible(false);
        kol_id.setVisible(false);
        kol_nr.setVisible(false);
        kol_pesel.setVisible(false);
        kol_adres.setVisible(false);

        if(check_imie.isSelected()) kol_imie.setVisible(true);
        if(check_nazwisko.isSelected()) kol_nazwisko.setVisible(true);
        if(check_id.isSelected()) kol_id.setVisible(true);
        if(check_numer.isSelected()) kol_nr.setVisible(true);
        if(check_pesel.isSelected()) kol_pesel.setVisible(true);
        if(check_adres.isSelected()) kol_adres.setVisible(true);

        kol_imie.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("name"));
        kol_id.setCellValueFactory(new PropertyValueFactory<ClientsTable,Integer>("id"));
        kol_nazwisko.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("surname"));
        kol_nr.setCellValueFactory(new PropertyValueFactory<ClientsTable,Integer>("nrKonta"));

        kol_pesel.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("pesel"));
        kol_adres.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("address"));

        tabela.setItems(items2);
    }



    public void searchingDisplay() {

        ObservableList<ClientsTable> items2 = FXCollections.observableArrayList ();

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 0) {

            int temp[] = SearchEngine.searchByName(Main.Base, search.getText());

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                for (int k = 0; k < temp.length; k++) {

                    if (Main.Base.getClient(i).getID() == temp[k]) {

                        items2.add(new ClientsTable(Main.Base.getClient(i)));
                    }
                }
            }
        }

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 1) { //Nazwisko

            int temp[] = SearchEngine.searchBySurname(Main.Base, search.getText());

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                for (int k = 0; k < temp.length; k++) {

                    if (Main.Base.getClient(i).getID() == temp[k]) {

                        items2.add(new ClientsTable(Main.Base.getClient(i)));
                    }
                }
            }
        }

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 2) { //Pesel

            int temp = SearchEngine.searchIdByPesel(Main.Base, search.getText());

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                if (Main.Base.getClient(i).getID() == temp) {

                    items2.add(new ClientsTable(Main.Base.getClient(i)));
                }
            }
        }

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 3) { //Adres

            int temp[] = SearchEngine.searchByAddress(Main.Base, search.getText());

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                for (int k = 0; k < temp.length; k++) {

                    if (Main.Base.getClient(i).getID() == temp[k]) {

                        items2.add(new ClientsTable(Main.Base.getClient(i)));
                    }
                }
            }
        }

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 4) { //Numer konta

            int temp = SearchEngine.searchIdByAccountNumber(Main.Base, Integer.parseInt(search.getText()));

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                if (Main.Base.getClient(i).getID() == temp) {

                    items2.add(new ClientsTable(Main.Base.getClient(i)));
                }
            }
        }

        if(wyszukiwanie_opcje.getSelectionModel().getSelectedIndex() == 5) { //Numer id

            int temp = Integer.parseInt(search.getText());

            for (int i = 0; i < Main.Base.getNumberOfClients(); i++) {

                if (Main.Base.getClient(i).getID() == temp) {

                    items2.add(new ClientsTable(Main.Base.getClient(i)));
                    break;
                }


            }
        }

        kol_imie.setVisible(false);
        kol_nazwisko.setVisible(false);
        kol_id.setVisible(false);
        kol_nr.setVisible(false);
        kol_pesel.setVisible(false);
        kol_adres.setVisible(false);

        if(check_imie.isSelected()) kol_imie.setVisible(true);
        if(check_nazwisko.isSelected()) kol_nazwisko.setVisible(true);
        if(check_id.isSelected()) kol_id.setVisible(true);
        if(check_numer.isSelected()) kol_nr.setVisible(true);
        if(check_pesel.isSelected()) kol_pesel.setVisible(true);
        if(check_adres.isSelected()) kol_adres.setVisible(true);

        kol_imie.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("name"));
        kol_id.setCellValueFactory(new PropertyValueFactory<ClientsTable,Integer>("id"));
        kol_nazwisko.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("surname"));
        kol_nr.setCellValueFactory(new PropertyValueFactory<ClientsTable,Integer>("nrKonta"));
        kol_pesel.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("pesel"));
        kol_adres.setCellValueFactory(new PropertyValueFactory<ClientsTable,String>("address"));

        tabela.setItems(items2);
    }

    public void copiesDisplay() {

        ObservableList<String> items = FXCollections.observableArrayList ();

        File folder = new File("DataBase/Backup/");
        File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {

                if (listOfFiles[i].isFile()) {

                    items.add(listOfFiles[i].getName());
                }
            }

        FXCollections.sort(items);
        FXCollections.reverse(items);
        lista.setItems(items);

    }

    public void loadBackup() throws IOException, ClassNotFoundException {

        Main.Base.loadDataBase("Backup/" + lista.getSelectionModel().getSelectedItem());
        System.out.println(lista.getSelectionModel().getSelectedItem());
    }

    public void myInfo() throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_client_side_info.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.info_dane.setText(info.getText());
        controller.id_dane.setText(idPole.getText());
        controller.imie.setText(Main.Base.getClientByID(Main.ID).getName());
        controller.nazwisko.setText(Main.Base.getClientByID(Main.ID).getSurname());
        controller.adres.setText(Main.Base.getClientByID(Main.ID).getAddress());
        controller.pesel.setText(Main.Base.getClientByID(Main.ID).getPesel());

        StringBuilder sb = new StringBuilder();

        sb.append(Main.ID);
        String strInt = sb.toString();
        controller.id_klienta.setText(strInt);

        sb = new StringBuilder();
        sb.append(Main.Base.getClientByID(Main.ID).getAccount().getAccountNumber());
        strInt = sb.toString();
        controller.numer_konta_dane.setText(strInt);

        Main.window.setScene(new Scene(parent));
    }

    public void history() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/scene_client_side_history.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.info_historia.setText(info.getText());
        controller.id_historia.setText(idPole.getText());
        controller.historia.setText(Main.Base.getClientByID(Main.ID).getAccount().getAccountHistory());

        Main.window.setScene(new Scene(parent));
    }

    public void display(String title, String message) throws IOException {
        Main.window_new = new Stage();
        Main.window_new.initModality(Modality.APPLICATION_MODAL);
        Main.window_new.setTitle(title);
        Main.window_new.setResizable(false);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/ui.scenes/confirm.fxml"));
        Parent parent = loader.load();
        Controller controller = (Controller) loader.getController();
        controller.pytanie.setText(message);

        Main.window_new.setScene(new Scene(parent));
        Main.window_new.showAndWait();
    }

    public void yes() {

        answer = true;
        Main.window_new.close();
    }

    public void no() {

        answer = false;
        Main.window_new.close();
    }
}

