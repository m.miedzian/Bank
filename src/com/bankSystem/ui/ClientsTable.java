package com.bankSystem.ui;

import java.io.Serializable;

import com.bankSystem.model.Client;

public class ClientsTable {

    private String 	name;
    private String surname;
    private Integer id;
    private String pesel;
    private String address;
    private Integer nrKonta;



    public ClientsTable(Client object) {

        this.name=object.getName();
        this.surname=object.getSurname();


        this.id=object.getID();

        this.pesel=object.getPesel();
        this.address=object.getAddress();

        this.nrKonta=object.getAccount().getAccountNumber();


    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getNrKonta() {
        return this.nrKonta;
    }

    public String getPesel() {
        return this.pesel;
    }

    public String getAddress() {
        return this.address;
    }
}
