package com.bankSystem.logic;

import com.bankSystem.data.DataBaseInFile;

public class SearchEngine {

    public static int searchIdByAccountNumber(DataBaseInFile currentBase,int searchNumber) {

        for(int i=0; i<currentBase.getNumberOfClients(); i++) {
            if(searchNumber == currentBase.getClient(i).getAccount().getAccountNumber()) {
                return currentBase.getClient(i).getID();

            }

        }


        return -1; // Zwraca -1 jeżeli no znajdzie konta w bazie
    }

    public static int searchIdByPesel(DataBaseInFile currentBase,String searchPesel) {
        for(int i=0; i<currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getPesel().compareTo(searchPesel) == 0) {
                return currentBase.getClient(i).getID();
            }
        }
        return -1; // Zwraca -1 jeżeli no znajdzie konta w bazie
    }

    public static int[] searchByName (DataBaseInFile currentBase, String name) {

        int numberOfRecords = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getName().equalsIgnoreCase(name)) {
                numberOfRecords++;
            }
        }
        int[] tempArray = new int[numberOfRecords];

        int temp = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getName().equalsIgnoreCase(name)) {
                tempArray[temp]=currentBase.getClient(i).getID();
                temp++;
            }
        }

        return tempArray;
    }

    public static int[] searchBySurname (DataBaseInFile currentBase, String surname) {

        int numberOfRecords = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getSurname().equalsIgnoreCase(surname)) {
                numberOfRecords++;
            }
        }
        int[] tempArray = new int[numberOfRecords];

        int temp = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getSurname().equalsIgnoreCase(surname)) {
                tempArray[temp]=currentBase.getClient(i).getID();
                temp++;
            }
        }

        return tempArray;
    }

    public static int[] searchByAddress (DataBaseInFile currentBase, String Address) {

        int numberOfRecords = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getAddress().equalsIgnoreCase(Address)) {
                numberOfRecords++;
            }
        }
        int[] tempArray = new int[numberOfRecords];

        int temp = 0;
        for(int i=0; i < currentBase.getNumberOfClients(); i++) {
            if(currentBase.getClient(i).getAddress().equalsIgnoreCase(Address)) {
                tempArray[temp]=currentBase.getClient(i).getID();
                temp++;
            }
        }

        return tempArray;
    }
}