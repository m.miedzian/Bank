package com.bankSystem.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import com.bankSystem.data.DataBaseInFile;
import com.bankSystem.data.ThreadForBackup;


public class Main extends Application  {


    public static Stage window,window_new;
    public static Scene firstScene;

    public static int ID;
    public static DataBaseInFile Base = new DataBaseInFile();


    @Override
    public void start(Stage primaryStage) throws Exception{

        window=primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/ui.scenes/scene_index.fxml"));

        window.setTitle("System Bankowy by MieO v1.0");
        window.setScene(new Scene(root));
        window.show();
        window.setResizable(false);
        
        

    }



    public static void main(String[] args) throws IOException, ClassNotFoundException {
       ThreadForBackup Backup = new ThreadForBackup(Base,10);
        Base.loadDataBase("Base.dat");


        //Base.getClientByID(1).getAccount().paymentOnAccount(1000);
/*
    Base.addClient("Janusz", "Kowalewski","95062903754", "Lodz");
        Base.addClient("Iwona", "Kowal", "66021812345", "Krakow");
        Base.addClient("Mariola", "Brzeczyszczykiewicz", "75091823898", "Warszawa");
        Base.addClient("Michal", "Adamiak", "94092701568", "Krakow");
        Base.addClient("Katarzyna", "Foremniak", "90021715862","Zakopane");
        Base.addClient("Monika", "Jedrzejek", "95042714785", "Pabianice");
        Base.addClient("Janusz", "Mosiolek", "44090126547", "Czestochowa");
        Base.addClient("Jerzy", "Dokimuk", "50122623458", "Lodz");
        Base.addClient("Grzegorz", "Hajto", "75041923486", "Gdansk");
        Base.addClient("Monika", "Ruda", "99080515263", "Ksawerow");
        Base.addClient("Janusz", "Zbarski", "65100556879", "Lodz");
        Base.addClient("Dariusz", "Michalczewski", "59021445784", "Radom");
        Base.addClient("Izabela", "Drawska", "68111245712", "Lublin");
        Base.addClient("Szymon", "Mantaj", "90121486465", "Ksawerow");
        Base.addClient("Mateusz", "Bozek", "74030656789", "Lodz");
        Base.addClient("Patrycja", "Szczawinska", "68092154879", "Warszawa");
        Base.addClient("Wladyslaw", "Bogon", "98091445687", "Lodz");
        Base.addClient("Spejson", "Ekipa", "70030365484", "Zakopane");
        Base.addClient("Marcin", "Chabasinski", "78091845684", "Radom");
        Base.addClient("Magdalena", "Chojnacka", "69041545687", "Gliwice");
        Base.addClient("Mateusz", "Ryba", "74120478569", "Poznan");
        Base.addClient("Sandra", "Ryba", "79060754879", "Wladyslawowo");
        Base.addClient("Janusz", "Gorzelski", "63041678412", "Szczecin");
        Base.addClient("Filip", "Kubski", "90091546845", "Lodz");
        Base.addClient("Kinga", "Rybicka", "95101944865", "Warszawa");
        Base.addClient("Piotr", "Wyszomirski", "83070841235", "Konin");
        Base.addClient("Adam", "Wladam", "88091745684", "Lodz");
        Base.addClient("Anna", "Kucharska", "84041446587", "Skierniewice");
        Base.addClient("Tomasz", "Pasieka", "83071015486", "Strykow");

        Base.saveDataBase("Base.dat");
*/
        //System.out.println();
        launch(args);
        Base.saveDataBase("Base.dat");
        System.out.println("Baza zapisana");
        Backup.stopThread();
        System.out.println("Watek backup zamkniety");
        


    }
}
